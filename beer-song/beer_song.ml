open Core.Std

let verse = function
  | 1 -> "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n"
  | 0 -> "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n"
  | 2 -> "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n"
  | n ->
    let n_str = Int.to_string n in
    let n_str_dec = Int.to_string (n - 1) in
    n_str ^ " bottles of beer on the wall, " ^ n_str ^ " bottles of beer.\n" ^
    "Take one down and pass it around, " ^ n_str_dec ^ " bottles of beer on the wall.\n"

let sing ~from ~until =
  List.range from until ~stride:(-1) ~stop:`inclusive
  |> List.map ~f:verse
  |> List.fold ~init:"" ~f:(fun x y -> x ^ y ^ "\n")
