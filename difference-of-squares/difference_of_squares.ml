open Core.Std

let square_of_sum n =
  Sequence.range 0 n ~stop:`inclusive
  |> Sequence.fold ~init:0 ~f:(+)
  |> (fun x -> Int.pow x 2)

let sum_of_squares n =
  Sequence.range 0 n ~stop:`inclusive
  |> Sequence.map ~f:(fun x -> Int.pow x 2)
  |> Sequence.fold ~init:0 ~f:(+)

let difference_of_squares n =
  square_of_sum n - sum_of_squares n
