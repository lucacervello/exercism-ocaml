open Core.Std

let rec fold  ~(init: 'acc) ~(f: 'acc -> 'a -> 'acc) (lst : 'a list) : 'acc =
  match lst with
  | [] -> init
  | x :: xs -> fold ~init:(f init x) ~f xs

let length (lst : 'a list) : int =
   fold ~init:0 ~f:(fun acc _ -> acc + 1) lst

let reverse (lst : 'a list) : 'a list =
   fold ~init:[] ~f:(fun acc x -> x :: acc) lst

let map ~f:(f : ('a -> 'b)) (lst : 'a list) : 'b list =
  fold ~init:[] ~f:(fun acc x -> (f x) :: acc) lst |> reverse

let filter ~f:(f : 'a -> bool) (lst : 'a list) : 'a list =
  fold ~init:[] ~f:(fun acc x -> if (f x) then x :: acc else acc) lst |> reverse

let append (lst1 : 'a list) (lst2 : 'a list) : 'a list =
  fold ~init:lst2 ~f:(fun acc x -> x :: acc) (reverse lst1)

let concat (lst : 'a list list) : 'a list =
  fold ~init:[] ~f:(fun acc x -> append x acc) (reverse lst)
