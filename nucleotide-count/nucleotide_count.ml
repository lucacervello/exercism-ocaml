open Core.Std

let count str ch =
  String.count ~f:(Char.equal ch) str

let nucleotide_counts str =

  let add acc key =
    let data = count str key in
    if data = 0
    then acc
    else Char.Map.add acc ~key ~data
  in

  ['A';'G';'T';'C']
  |> List.fold ~init:Char.Map.empty ~f:add
