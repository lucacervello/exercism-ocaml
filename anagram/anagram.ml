open Core.Std

let anagram str1 str2 =
  let str_sort s =
    s
    |> String.to_list
    |> List.sort ~cmp:Char.compare
    |> List.to_string ~f:Char.to_string
  in

  let str1l, str2l = String.lowercase str1, String.lowercase str2 in

  if String.equal str1l str2l
  then
    false
  else
    String.equal (str_sort str1l) (str_sort str2l)

let anagrams str lstString =
  List.filter ~f:(anagram str) lstString
