open Core.Std

module StringMap = String.Map

let word_count str =

  let normalize x =
    match x with
    | x when Char.is_alphanum x -> Char.lowercase x
    | _ -> ' '
  in

  let string_to_list s =
    s
    |> String.map ~f:normalize
    |> String.split ~on:' '
    |> List.filter ~f:(fun x -> not (String.is_empty x))
  in

  let rec word_count_acc map lst =
    match lst with
    | [] -> map
    | x :: xs ->
      word_count_acc (StringMap.update map x ~f:(fun k -> Option.value_map k ~default:1 ~f:((+) 1))) xs
  in

  word_count_acc StringMap.empty (string_to_list str)
